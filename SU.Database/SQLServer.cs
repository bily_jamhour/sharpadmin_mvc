﻿using System;
using System.Collections.Generic;
//es necesario el data.sqlclient para conectarnos al SQL
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
//using System.Data.Odbc;
//para poder aplicar esta clase a nuestro proyecto hemos hecho lo siguiente:
/*
 * PASOS A SEGUIR
1)  Segundo boton en la solucion , Agregar -->Nuevo proyecto (Visual C#)-->Biblioteca de clases
2)  Creamos una clase si no existe, (es este fichero) 
3) Aplicamos las funciones necesarias
4) Por ultimo nos dirigimos al explorador de soluciones debajo de solución(SharpUpdates: 2ndo boton--> Agregar-->Referencia
 */
namespace SU.Database
{
    public class SQLServer //La funcion principal de la clase contendra el mismo nombre del fichero.
    {
        /// <summary>
        //Encrypting a string

        public static string passwordEncrypt(string inText, string key)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(inText);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    inText = Convert.ToBase64String(mStream.ToArray());
                }
            }
            return inText;
        }

        //Decrypting a string
        public static string passwordDecrypt(string cryptTxt, string key)
        {
            cryptTxt = cryptTxt.Replace(" ", "+");
            byte[] bytesBuff = Convert.FromBase64String(cryptTxt);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    cryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return cryptTxt;
        }

        /// </summary>
        private string connectionString; //creamos una variable privada (parecido a PHP) en la cual contendra la conexión a la BBDD (credenciales incluidas)

        public SQLServer(string conexion) //ES EL CONTRUCTOR DE LA CLASE!!! Funcion sencilla en la que igualamos la variable protegida a la variable conexion
        { //esta funcion es el contruct ya que tiene el mismo nombre que la clase y fichero en cuestión.
            connectionString = conexion;
        }

        public void EjecutarSQL(string sentencia)
        { //funcion para ejecutar sentencias sql sin llevar parametros espeficicos, no devuelve ningun valor (void)
            try
            {


                using (SqlConnection conexion = new SqlConnection(connectionString))
                { // usamos una propiedad que tiene Data.SqlClient que es SqlConnection en la cual creamos un objeto de ese tipo con el contenido que haya
                  //en la variable protegida conectionString (dirigirse al fichero Wb.config para ver el contenido de conectionString)

                    using (SqlCommand cmd = conexion.CreateCommand())
                    {//una vez creado el objeto conexion (de tipo Sqlconnection), usando otra propiedadad llamada SqlCommand, dirigiremos
                     //nuestros comandos al objeto conexion, que es el que conecta a nuestra BBDD (por esa razon la variable cmd es igualada a otra propiedad de SqlConection)
                        conexion.Open();
                        //conexion al ser del tipo SqlConnection, podremos acceder al metodo Open() para iniciar la conexion a la base de datos
                        cmd.CommandText = sentencia;
                        //un vez inicializada, accedemos al metodo CommandText ya que el objeto cmd es del tipo SqlCommand y es igualada a la variable que contendra el comando SQL (se la pasamos a la funcion)
                        cmd.ExecuteNonQuery();
                        //finalmente ejecuta la consulta en la BBDD devolviendo las filas afectadas
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public int GetRowNumSQL(string sentencia)
        {
            using (SqlConnection conexion = new SqlConnection(connectionString))
            { // usamos una propiedad que tiene Data.SqlClient que es SqlConnection en la cual creamos un objeto de ese tipo con el contenido que haya
                //en la variable protegida conectionString (dirigirse al fichero Wb.config para ver el contenido de conectionString)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//una vez creado el objeto conexion (de tipo Sqlconnection), usando otra propiedadad llamada SqlCommand, dirigiremos
                    //nuestros comandos al objeto conexion, que es el que conecta a nuestra BBDD (por esa razon la variable cmd es igualada a otra propiedad de SqlConection)
                    conexion.Open();
                    //conexion al ser del tipo SqlConnection, podremos acceder al metodo Open() para iniciar la conexion a la base de datos
                    cmd.CommandText = sentencia;
                    //un vez inicializada, accedemos al metodo CommandText ya que el objeto cmd es del tipo SqlCommand y es igualada a la variable que contendra el comando SQL (se la pasamos a la funcion)
                    return Convert.ToInt32(cmd.ExecuteScalar());
                    //finalmente ejecuta la consulta en la BBDD devolviendo las filas afectadas
                }
            }
        }

        public void PrepareParametersSQL(Type t, ref string[] sAllValuesParam, ref string[] sAllNamesParam, object Model)
        {
            IList<PropertyInfo> props = new List<PropertyInfo>(t.GetProperties());
            sAllValuesParam = new string[props.Count];
            sAllNamesParam = new string[props.Count];

            int x = 0;
            foreach (PropertyInfo prop in props)
            {

                object propValue = prop.GetValue(Model, null);
                sAllNamesParam[x] = prop.Name;
                sAllValuesParam[x] = Convert.ToString(propValue);
                x++;

            }

        }
        public void EjecutarSQL(string sentencia, string[] nombreparametros, object[] valoresparametros)
        { //esta funcion es muy similar a la anterior, con la diferencia que nos llevamos los parametros a consultar en la sentencia
            using (SqlConnection conexion = new SqlConnection(connectionString))
            {//creamos un objeto del tipo SqlConnection que tendra como valor la conexion indicada en la variable connectionString(ver fichero Web.config)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//usamos un objeto del tipo SqlCommand igualandolo al objeto creado antes y accedemos a su metodo CreateCommand()
                    conexion.Open(); //iniciamos la conexion
                    cmd.CommandText = sentencia; //mediante el objeto del tipo SqlCommand, accedemos a la propiedad CommandText y lo igualamos a la sentencia

                    for (int i = 0; i < nombreparametros.Length; i++)
                    {

                        if (!string.IsNullOrEmpty(nombreparametros[i])) //si el contenido en aquella posicion no es nulo o vacio...
                        {
                            cmd.Parameters.AddWithValue(nombreparametros[i], valoresparametros[i]);
                            //del objeto cmd, accedemos a la propiedad parametros y usamos el metodo AddWithValues, para añadir la clave y valor del parametro al objeto cmd
                        }
                    }
                    //una vez acabado tendremos el objeto cmd, por un lado con la sentencia (commandText) y por otro lado los parametros (parameters) para llevarla a cabo
                    cmd.ExecuteNonQuery();
                    conexion.Close();
                    //Al igual que en la anterior funcion ejecutara la query mediante el objeto cmd
                }
            }
        }

        public List<Dictionary<string, object>> LeerDatos(string sentencia, string[] nombreparametros, object[] valoresparametros)
        {//creamos una funcion en la cual devolverá un array de objetos (por esta razon se le añade todo eso del Linst<Dictionary>,etc
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            //creamos un array de objetos llamado data, este sera encargado de ir guardando la informacion que recorramos y el que devolveremos
            using (SqlConnection conexion = new SqlConnection(connectionString))
            { //usando la clase SqlConnection crearemos un objeto llamado conexion con los valores de conexion especificados en la variable connectionString (web.config)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//usando la clase SqlCommand  creamos un variable llamada cmd en la cual contendra el metodo CreateCommand del objeto conexion
                    conexion.Open(); //inciamos la conexion
                    cmd.CommandText = sentencia; //al igualar la variable al metodo CreateCommand, podemos acceder a submetodos, en este caos CommandText y es igualado a nuestra sentencia



                    for (int i = 0; i < nombreparametros.Length; i++)
                    {

                        if (!string.IsNullOrEmpty(nombreparametros[i])) //si el contenido en aquella posicion no es nulo o vacio...
                        {
                            cmd.Parameters.AddWithValue(nombreparametros[i], valoresparametros[i]);
                            //del objeto cmd, accedemos a la propiedad parametros y usamos el metodo AddWithValues, para añadir la clave y valor del parametro al objeto cmd
                        }
                    }



                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {//usamos otra clase de SqlClient, SqlDataReader aplicandosela a una variable "dr" y se igualara al contenido que devuelva el metodo ExecuteReader de la variable cmd

                        while (dr.Read()) //accedemos al metodo Read de la variable dr (ya que es del tipo SqlDataReader)
                        {//mientras haya contenido en dr

                            Dictionary<string, object> item = new Dictionary<string, object>();
                            //creamos un segundo array de objetos llamado item.
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                item[dr.GetName(i)] = dr.GetValue(i); //añadimos al array item como clave el nombre del campo y como valor su contenido
                            }//una vez acabado tendremos el codigo (id) del recien creado Releases
                            data.Add(item);//finalmente añadimos todo el array de objetos item al array de objetos data (creado al principio de la funcion 

                        }
                    }
                }
            }
            return data; //finalmente devolvemos el array de objetos. IR al fichero managecontroller para ver como se les llama a la funcion y recibir el contenido !!
        }

        public List<Dictionary<string, object>> LeerDatos(string sentencia)
        {//creamos una funcion en la cual devolverá un array de objetos (por esta razon se le añade todo eso del Linst<Dictionary>,etc
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            //creamos un array de objetos llamado data, este sera encargado de ir guardando la informacion que recorramos y el que devolveremos
            using (SqlConnection conexion = new SqlConnection(connectionString))
            { //usando la clase SqlConnection crearemos un objeto llamado conexion con los valores de conexion especificados en la variable connectionString (web.config)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//usando la clase SqlCommand  creamos un variable llamada cmd en la cual contendra el metodo CreateCommand del objeto conexion
                    conexion.Open(); //inciamos la conexion
                    cmd.CommandText = sentencia; //al igualar la variable al metodo CreateCommand, podemos acceder a submetodos, en este caos CommandText y es igualado a nuestra sentencia
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {//usamos otra clase de SqlClient, SqlDataReader aplicandosela a una variable "dr" y se igualara al contenido que devuelva el metodo ExecuteReader de la variable cmd

                        while (dr.Read()) //accedemos al metodo Read de la variable dr (ya que es del tipo SqlDataReader)
                        {//mientras haya contenido en dr

                            Dictionary<string, object> item = new Dictionary<string, object>();
                            //creamos un segundo array de objetos llamado item.
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                item[dr.GetName(i)] = dr.GetValue(i); //añadimos en su clase el nombre y en su valor el contenido
                            }//una vez acabado tendremos un array con todas las filas que ha leido
                            data.Add(item);//finalmente añadimos todo el array de objetos item al array de objetos data (creado al principio de la funcion 

                        }
                    }
                }
            }
            return data; //finalmente devolvemos el array de objetos. IR al fichero managecontroller para ver como se les llama a la funcion y recibir el contenido !!
        }
        public object GetScalar(string sentencia)
        {//en el caso que tengamos que usar solamente la sentencia sin where...
            List<string> Nombres = new List<string>();
            List<object> Valores = new List<object>();
            return GetScalar(sentencia, Nombres.ToArray(), Valores.ToArray());
            //return GetScalar(sentencia,new string [1],new object [1]);
        }

        public object GetScalar(string sentencia, string[] nombresparametros, object[] valoresparametros)
        { //esta funcion es muy similar a la anterior, con la diferencia que nos llevamos los parametros a consultar en la sentencia
            using (SqlConnection conexion = new SqlConnection(connectionString))
            {//creamos un objeto del tipo SqlConnection que tendra como valor la conexion indicada en la variable connectionString(ver fichero Web.config)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//usamos un objeto del tipo SqlCommand igualandolo al objeto creado antes y accedemos a su metodo CreateCommand()
                    conexion.Open(); //iniciamos la conexion
                    cmd.CommandText = sentencia; //mediante el objeto del tipo SqlCommand, accedemos a la propiedad CommandText y lo igualamos a la sentencia

                    for (int i = 0; i < nombresparametros.Length; i++)
                    {

                        if (!string.IsNullOrEmpty(nombresparametros[i])) //si el contenido en aquella posicion no es nulo o vacio...
                        {
                            cmd.Parameters.AddWithValue(nombresparametros[i], valoresparametros[i]);
                            //del objeto cmd, accedemos a la propiedad parametros y usamos el metodo AddWithValues, para añadir la clave y valor del parametro al objeto cmd
                        }
                    }
                    return cmd.ExecuteScalar();
                    //Al igual que en la anterior funcion ejecutara la query mediante el objeto cmd
                }
            }
        }

        internal class Helper
        {
            public static string CreateKey()
            {
                string originalKey = "bilykarbias24071993";
                SHA1 sha1 = new SHA1CryptoServiceProvider();

                byte[] inputBytes = (new UnicodeEncoding()).GetBytes(originalKey);
                byte[] hash = sha1.ComputeHash(inputBytes);

                return Convert.ToBase64String(hash);
            }
        }


        public bool Autenticar(string usuario, string password, string sentencia, ref string Contrasenya)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sentencia, conn);
                command.CommandText = sentencia;
                //command.Parameters.AddWithValue("@USUARIO", usuario);

                string hash = passwordEncrypt(password, Helper.CreateKey()); //Helper.EncodePassword(string.Concat(usuario, password));
                command.Parameters.AddWithValue("@password", hash);

                int count = Convert.ToInt32(command.ExecuteScalar());
                if (count == 0)
                {
                    return false;
                }
                else
                {
                    Contrasenya = hash;
                    return true;
                }

            }
        }

        public bool ComprobarDatosColumna(string Tabla, Dictionary<string, object> Valores)
        {//FUNCION QUE PASADA LA TABLA DONDE SE HARA INSERT/UPDATE VERIFICAREMOS EL VALOR INTRODUCIDO EN CIERTA COLUMNA SI CUMPLE CON EL TIPO DE DATO ADMITIDO EN BBDD
            using (SqlConnection conexion = new SqlConnection(connectionString))
            { // usamos una propiedad que tiene Data.SqlClient que es SqlConnection en la cual creamos un objeto de ese tipo con el contenido que haya
                //en la variable protegida conectionString (dirigirse al fichero Wb.config para ver el contenido de conectionString)

                using (SqlCommand cmd = conexion.CreateCommand())
                {//una vez creado el objeto conexion (de tipo Sqlconnection), usando otra propiedadad llamada SqlCommand, dirigiremos
                    //nuestros comandos al objeto conexion, que es el que conecta a nuestra BBDD (por esa razon la variable cmd es igualada a otra propiedad de SqlConection)
                    conexion.Open();
                    //conexion al ser del tipo SqlConnection, podremos acceder al metodo Open() para iniciar la conexion a la base de datos
                    cmd.CommandText = string.Format("SELECT * FROM {0} WHERE 1=0;", Tabla);
                    //un vez inicializada, accedemos al metodo CommandText ya que el objeto cmd es del tipo SqlCommand y es igualada a la variable que contendra el comando SQL (se la pasamos a la funcion)

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {//usamos otra clase de SqlClient, SqlDataReader aplicandosela a una variable "dr" y se igualara al contenido que devuelva el metodo ExecuteReader de la variable cmd

                        while (dr.Read()) //accedemos al metodo Read de la variable dr (ya que es del tipo SqlDataReader)
                        {//mientras haya contenido en dr

                        }
                    }
                }
            }
            return false;
        }
    }

}
