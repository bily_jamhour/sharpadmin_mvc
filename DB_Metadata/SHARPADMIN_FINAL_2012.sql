USE [master]
GO
/****** Object:  Database [SharpAdmin]    Script Date: 26/04/2016 17:14:04 ******/
CREATE DATABASE [SharpAdmin]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SharpAdmin', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SharpAdmin.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SharpAdmin_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SharpAdmin_log.ldf' , SIZE = 3072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SharpAdmin] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SharpAdmin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SharpAdmin] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SharpAdmin] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SharpAdmin] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SharpAdmin] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SharpAdmin] SET ARITHABORT OFF 
GO
ALTER DATABASE [SharpAdmin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SharpAdmin] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [SharpAdmin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SharpAdmin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SharpAdmin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SharpAdmin] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SharpAdmin] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SharpAdmin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SharpAdmin] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SharpAdmin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SharpAdmin] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SharpAdmin] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SharpAdmin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SharpAdmin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SharpAdmin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SharpAdmin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SharpAdmin] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SharpAdmin] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SharpAdmin] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SharpAdmin] SET  MULTI_USER 
GO
ALTER DATABASE [SharpAdmin] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SharpAdmin] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SharpAdmin] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SharpAdmin] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [SharpAdmin]
GO
/****** Object:  User [BILY]    Script Date: 26/04/2016 17:14:04 ******/
CREATE USER [BILY] FOR LOGIN [BILY] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [BILY]
GO
/****** Object:  Table [dbo].[ALMACEN]    Script Date: 26/04/2016 17:14:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ALMACEN](
	[NOMBRE] [nchar](30) NOT NULL,
	[PRECIO_INICIAL] [money] NOT NULL,
	[IMAGEN] [nchar](20) NULL,
	[DESCRIPCION] [nchar](50) NULL,
	[IDPRODUCTO] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ALMACEN] PRIMARY KEY CLUSTERED 
(
	[IDPRODUCTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ROLES]    Script Date: 26/04/2016 17:14:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROLES](
	[ROL] [tinyint] NOT NULL,
	[DESCRIPCION] [nchar](50) NULL,
 CONSTRAINT [PK_ROLES] PRIMARY KEY CLUSTERED 
(
	[ROL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STOCK]    Script Date: 26/04/2016 17:14:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STOCK](
	[IDTIENDA] [int] NOT NULL,
	[IDPRODUCTO] [int] NOT NULL,
	[PRECIOTOTAL] [money] NOT NULL,
	[ESTADO] [bit] NOT NULL,
	[FECHA] [timestamp] NOT NULL,
	[IDSTOCK] [int] IDENTITY(1,1) NOT NULL,
	[FECHA_DISPONIBLE] [datetime] NULL,
 CONSTRAINT [PK_STOCK] PRIMARY KEY CLUSTERED 
(
	[IDSTOCK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TIENDA]    Script Date: 26/04/2016 17:14:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIENDA](
	[NOMBRE] [nchar](20) NOT NULL,
	[DIRECCION] [nchar](40) NULL,
	[TELEFONO] [numeric](9, 0) NULL,
	[CORREO] [nchar](40) NULL,
	[IDTIENDA] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_TIENDA] PRIMARY KEY CLUSTERED 
(
	[IDTIENDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USUARIOS]    Script Date: 26/04/2016 17:14:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUARIOS](
	[USUARIO] [nchar](15) NOT NULL,
	[NOMBRE] [nchar](15) NOT NULL,
	[DISABLED] [bit] NOT NULL,
	[ROL] [tinyint] NOT NULL,
	[IDUSER] [int] IDENTITY(1,1) NOT NULL,
	[PASSWORD] [char](45) NOT NULL,
	[TELEFONO] [int] NULL,
 CONSTRAINT [PK_USUARIOS] PRIMARY KEY CLUSTERED 
(
	[IDUSER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ALMACEN] ON 

INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'OnePlus One 2k11              ', 249.9500, N'oneplusone.jpg      ', N'Movil 5,5" FullHD, Snapdragon 801, 3Gb Ram        ', 6)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Macarrones 1Kg                ', 0.8000, N'macarrones.png      ', N'Macarrones Normales 1Kg.                          ', 7)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Spaghettis 500Gr              ', 1.1500, N'0092743.jpg         ', N'Paquete de 500Gr de Spaghettis                    ', 8)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Salsa de Tomate Frito         ', 0.8900, NULL, N'Pack de 3u de Salsa de Tomate Frito               ', 9)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Saco Cebolla 1Kg              ', 1.2000, NULL, N'Saco de Cebolla 1era Calidad 1Kg                  ', 10)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Lata de Atún Familiar         ', 4.4500, N'latafamiliar.png    ', N'Lata de Atún en Aceite Girasol de 1Kg             ', 11)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Latas de Atún                 ', 2.1700, NULL, N'Pack de 3u de Latas de Atún 67gr Aceite de Girasol', 12)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Latas de Maíz                 ', 1.1500, N'image-56.jpg        ', N'Pack de 3u de Latas de Maíz                       ', 13)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Motorola Moto G 3Gen          ', 152.9500, N'motog.jpg           ', N'Movil 5" HD, Snapdragon 410, 2Gb Ram,IPX7         ', 16)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Google Chromecast             ', 35.0000, N'chrome.jpg          ', N'ChromeCast para ver multimédia del móvil a la TV  ', 20)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Google Chromecast Audio       ', 30.0000, NULL, N'ChromeCast Audio para escuchar tu música.         ', 22)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'OnePlus 2                     ', 400.9900, NULL, N'Movil 5,5" FullHD, Snapdragon 810, 4Gb Ram.       ', 23)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Lechuga Iceberg 200g          ', 0.7000, NULL, N'Paquete de lechuga iceberg preparada de 200gr    .', 24)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Ensalada Gourmet 175 g        ', 1.0500, NULL, N'.                                                 ', 25)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Ensalada 4 Estaciones 250 g   ', 0.4000, NULL, N'Ensalada para consumir 4 estaciones de 250 gr .   ', 26)
INSERT [dbo].[ALMACEN] ([NOMBRE], [PRECIO_INICIAL], [IMAGEN], [DESCRIPCION], [IDPRODUCTO]) VALUES (N'Macarrones XXL 2Kg            ', 1.4900, NULL, N'Paquete de Macarrones extra grandes de 2 Kg.      ', 27)
SET IDENTITY_INSERT [dbo].[ALMACEN] OFF
INSERT [dbo].[ROLES] ([ROL], [DESCRIPCION]) VALUES (1, N'ADMINISTRATIVO                                    ')
INSERT [dbo].[ROLES] ([ROL], [DESCRIPCION]) VALUES (2, N'TÉCNICO                                           ')
INSERT [dbo].[ROLES] ([ROL], [DESCRIPCION]) VALUES (3, N'VISITANTE                                         ')
SET IDENTITY_INSERT [dbo].[STOCK] ON 

INSERT [dbo].[STOCK] ([IDTIENDA], [IDPRODUCTO], [PRECIOTOTAL], [ESTADO], [IDSTOCK], [FECHA_DISPONIBLE]) VALUES (6, 8, 1.0000, 1, 18, CAST(0x0000A5F400C5C5F4 AS DateTime))
INSERT [dbo].[STOCK] ([IDTIENDA], [IDPRODUCTO], [PRECIOTOTAL], [ESTADO], [IDSTOCK], [FECHA_DISPONIBLE]) VALUES (3, 20, 143.8000, 1, 19, CAST(0x0000A5F400C5C605 AS DateTime))
INSERT [dbo].[STOCK] ([IDTIENDA], [IDPRODUCTO], [PRECIOTOTAL], [ESTADO], [IDSTOCK], [FECHA_DISPONIBLE]) VALUES (3, 23, 339.7500, 1, 20, CAST(0x0000A5F400C5C605 AS DateTime))
SET IDENTITY_INSERT [dbo].[STOCK] OFF
SET IDENTITY_INSERT [dbo].[TIENDA] ON 

INSERT [dbo].[TIENDA] ([NOMBRE], [DIRECCION], [TELEFONO], [CORREO], [IDTIENDA]) VALUES (N'Components&Sound    ', N'AV.Ferrocarril Nº84                     ', CAST(936543200 AS Numeric(9, 0)), N'CandSBJC@prestashop.com                 ', 3)
INSERT [dbo].[TIENDA] ([NOMBRE], [DIRECCION], [TELEFONO], [CORREO], [IDTIENDA]) VALUES (N'Mercadona Sanvi     ', N'Calle Sant Boi Nº36                     ', CAST(936542309 AS Numeric(9, 0)), N'helpdesk@mercadona.com                  ', 4)
INSERT [dbo].[TIENDA] ([NOMBRE], [DIRECCION], [TELEFONO], [CORREO], [IDTIENDA]) VALUES (N'LIDL                ', N'Calle Badalona Nº18                     ', NULL, NULL, 5)
INSERT [dbo].[TIENDA] ([NOMBRE], [DIRECCION], [TELEFONO], [CORREO], [IDTIENDA]) VALUES (N'Caprabo             ', N'Calle Compte d''Urgell Nº23              ', CAST(93650003 AS Numeric(9, 0)), N'tuCaprabo@hotmail.com                   ', 6)
INSERT [dbo].[TIENDA] ([NOMBRE], [DIRECCION], [TELEFONO], [CORREO], [IDTIENDA]) VALUES (N'DominosPizza        ', N'Crta. d''Esplugues Nº71                  ', CAST(934743538 AS Numeric(9, 0)), NULL, 7)
SET IDENTITY_INSERT [dbo].[TIENDA] OFF
SET IDENTITY_INSERT [dbo].[USUARIOS] ON 

INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'BILY           ', N'Bilal Jamhour  ', 0, 1, 2, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'JOSE           ', N'Jose Cantero   ', 0, 3, 3, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'Maria          ', N'Maria Prieto   ', 1, 2, 4, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'Sheila         ', N'Sheila Martín  ', 0, 2, 5, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'Ismael         ', N'Ismael Morato  ', 0, 3, 6, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'CSHARP         ', N'C#USER         ', 0, 2, 7, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', 0)
INSERT [dbo].[USUARIOS] ([USUARIO], [NOMBRE], [DISABLED], [ROL], [IDUSER], [PASSWORD], [TELEFONO]) VALUES (N'Irene          ', N'Irene M.M      ', 1, 3, 8, N'ADF9lFt1jXq01Rcnqn6dqw==                     ', NULL)
SET IDENTITY_INSERT [dbo].[USUARIOS] OFF
ALTER TABLE [dbo].[STOCK]  WITH CHECK ADD  CONSTRAINT [FK_STOCK_ALMACEN] FOREIGN KEY([IDPRODUCTO])
REFERENCES [dbo].[ALMACEN] ([IDPRODUCTO])
GO
ALTER TABLE [dbo].[STOCK] CHECK CONSTRAINT [FK_STOCK_ALMACEN]
GO
ALTER TABLE [dbo].[STOCK]  WITH CHECK ADD  CONSTRAINT [FK_STOCK_TIENDA] FOREIGN KEY([IDTIENDA])
REFERENCES [dbo].[TIENDA] ([IDTIENDA])
GO
ALTER TABLE [dbo].[STOCK] CHECK CONSTRAINT [FK_STOCK_TIENDA]
GO
ALTER TABLE [dbo].[USUARIOS]  WITH CHECK ADD  CONSTRAINT [FK_USUARIOS_ROLES] FOREIGN KEY([ROL])
REFERENCES [dbo].[ROLES] ([ROL])
GO
ALTER TABLE [dbo].[USUARIOS] CHECK CONSTRAINT [FK_USUARIOS_ROLES]
GO
USE [master]
GO
ALTER DATABASE [SharpAdmin] SET  READ_WRITE 
GO
