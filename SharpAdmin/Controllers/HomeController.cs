﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SU.Database;
using SHA.Idioma;
namespace SharpAdmin.Controllers
{
    public class HomeController : Controller
    {

        private SQLServer Conection;
        public ActionResult Index()
        {
            List<Dictionary<string, object>> TodosLosUsuarios = new List<Dictionary<string, object>>();
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            TodosLosUsuarios = Conection.LeerDatos(string.Format("SELECT Usuario, Nombre, Disabled FROM {0} ORDER BY Nombre", "USUARIOS"));

            ViewBag.Pag = "I";
            ViewBag.ListadoUsers = TodosLosUsuarios.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Pag = "A";
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Pag = "C";
            return View();
        }

        public string Conexion() //ESTABLECES CONEXION DESDE LA LECTURA DEL FICHERO WEB.CONFIG
        {
            System.Configuration.Configuration Config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/SharpAdmin");
            System.Configuration.ConnectionStringSettings Conexion;
            Conexion = Config.ConnectionStrings.ConnectionStrings[System.Web.Configuration.WebConfigurationManager.AppSettings["CadenaConexion"]];
            return Conexion.ConnectionString;
        }
    }
}