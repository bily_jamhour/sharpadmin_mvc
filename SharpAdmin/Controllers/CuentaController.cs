﻿using SharpAdmin.Filters;
using SharpAdmin.Models;
using SU.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SHA.Idioma;

namespace SharpAdmin.Controllers
{
    [Autorizacion]
    public class CuentaController : Controller
    {
        private SQLServer Conection;
        // GET: Cuenta
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoadFormulario(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("InicioMal", "Intento de inicio de sesión fallido.");
                return View("ErrorContent");
            }
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);

            // No cuenta los errores de inicio de sesión para el bloqueo de la cuenta
            // Para permitir que los errores de contraseña desencadenen el bloqueo de la cuenta, cambie a shouldLockout: true
            //var result = await SignInManager.PasswordSignInAsync(model.Usuario, model.Password, model.RememberMe, shouldLockout: false);
            //ESTARIA BIEN CIFRAR EL TEXTO DE LA CONTRASEÑA INTRODUCIDA Y COMPARARLA EN BASE DE DATOS
            string pass = "";
            bool autenticado = Conection.Autenticar(model.Usuario, model.Password, string.Format("SELECT COUNT(*) FROM USUARIOS WHERE USUARIO='{0}' AND PASSWORD=@password AND DISABLED=0", model.Usuario), ref pass);
            if (autenticado)
            {
                //GENERAR COOKIE DE INICIO DE SESIÓN POR PARTE DEL USUARIO
                List<Dictionary<string, object>> Usuarios = new List<Dictionary<string, object>>();
                Usuarios = Conection.LeerDatos(string.Format("SELECT * FROM USUARIOS WHERE USUARIO='{0}' AND PASSWORD=@password AND DISABLED=0", model.Usuario), new string[] { "password" }, new object[] { pass });
                if (Usuarios.Count > 0)
                {
                    FormsAuthentication.SetAuthCookie("sharpUser", false); //creamos una cookie
                    Response.Cookies.Add(new HttpCookie("shlogin", "1")); //añadimos esa cookie a un mensaje del navegador llamado sulogin con un valor
                    HttpCookie MyCookie = Response.Cookies["shlogin"];
                    MyCookie.Values["NOMBRE"] = Convert.ToString(Usuarios[0]["NOMBRE"]);
                    MyCookie.Values["LOGIN"] = Convert.ToString(Usuarios[0]["USUARIO"]);
                    MyCookie.Values["ROL"] = Convert.ToString(Usuarios[0]["ROL"]);

                    return new JsonResult()
                    {
                        Data = new { Envio = true },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };

                    //  return RedirectToAction("Bienvenida", "Manage", new { Nombre = Usuarios[0]["NOMBRE"], Login = Usuarios[0]["USUARIO"], Rol = Usuarios[0]["ROL"] });//".Usuario, Rol = }); //new{Usuario=model.Usuario, Rol = );
                }
                else
                {

                    ModelState.AddModelError("InicioMal", "Intento de inicio de sesión fallido.");
                    return View("ErrorContent");
                }

            }
            else
            {
                ModelState.AddModelError("InicioMal", "Intento de inicio de sesión fallido.");
                return View("ErrorContent");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogOut()
        {
            //Request.Cookies.Remove("shlogin"); //LA ELIMINAMOS DEL 
            HttpCookie myCookie = new HttpCookie("shlogin");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            // Request.Cookies["shlogin"].Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);
            return RedirectToAction("Index", "Home");
        }
        public string Conexion() //ESTABLECES CONEXION DESDE LA LECTURA DEL FICHERO WEB.CONFIG
        {
            System.Configuration.Configuration Config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/SharpAdmin");
            System.Configuration.ConnectionStringSettings Conexion;
            Conexion = Config.ConnectionStrings.ConnectionStrings[System.Web.Configuration.WebConfigurationManager.AppSettings["CadenaConexion"]];
            return Conexion.ConnectionString;
        }

    }
}