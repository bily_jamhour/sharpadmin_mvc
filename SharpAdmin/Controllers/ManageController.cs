﻿using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SharpAdmin.Models;
using SharpAdmin.Filters;
using System.Collections.Generic;
using SU.Database;
using System.IO;
using System.ComponentModel.DataAnnotations;
using SHA.Idioma;
using System.Reflection;

namespace SharpAdmin.Controllers
{
    [Autorizacion]
    public class ManageController : Controller
    {
        private SQLServer Conection;
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Index(string Nombre, int Rol, string Login)
        //{
        //    return JsonResult JsonResult(Nombre, Rol, Login);
        //}
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(string Nombre, string Rol, string Login)
        {
            IndexViewModel Logueado = new IndexViewModel();
            if (Request.Cookies["shlogin"].HasKeys)
            {
                Logueado.Rol = Convert.ToInt32(Request.Cookies["shlogin"]["ROL"]);
                Logueado.NombreUser = Convert.ToString(Request.Cookies["shlogin"]["NOMBRE"]);
                Logueado.NombreLogin = Convert.ToString(Request.Cookies["shlogin"]["LOGIN"]);
            }
            ViewBag.StatusMessage = "Inicio de sesión satisfactorio. Puede proceder a administrar la BBDD.";
            return View("Index", Logueado);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetUsuarios(string RolUser)
        {

            StringBuilder SQL = new StringBuilder();
            SQL.AppendFormat("SELECT * FROM {0} ORDER BY NOMBRE", "USUARIOS");
            List<Dictionary<string, object>> USUARIOSTABLA = new List<Dictionary<string, object>>();
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            USUARIOSTABLA = Conection.LeerDatos(SQL.ToString());
            List<IndexViewModel> ListadoUsuario = new List<IndexViewModel>();

            foreach (var UsuarioCoger in USUARIOSTABLA)
            {
                IndexViewModel User = new IndexViewModel();
                User.Id = Convert.ToInt32(UsuarioCoger["IDUSER"]);
                User.NombreLogin = Convert.ToString(UsuarioCoger["USUARIO"]);
                User.NombreUser = Convert.ToString(UsuarioCoger["NOMBRE"]);
                User.Rol = Convert.ToInt32(UsuarioCoger["ROL"]);
                User.Disabled = Convert.ToBoolean(UsuarioCoger["DISABLED"]);
                User.Telefono = (UsuarioCoger["TELEFONO"] != DBNull.Value) ? Convert.ToInt32(UsuarioCoger["TELEFONO"]) : 000000000;
                ListadoUsuario.Add(User);
            }
            SQL.Clear();
            SQL.Append("SELECT * FROM ROLES");

            List<Dictionary<string, object>> Roles = new List<Dictionary<string, object>>();
            Roles = Conection.LeerDatos(SQL.ToString());
            ViewBag.Roles = Roles.ToList();
            ViewBag.RolActual = RolUser;
            return View(ListadoUsuario);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubirImagen(string archivo)
        {
            var fileName = string.Empty;
            string mensaje = string.Empty;
            bool hecho = false;
            Dictionary<string, object> imagen = new Dictionary<string, object>();
            try
            {
                /*PARA PODER ALMACENAR EL FICHERO DEBEMOS CREAR UNA AUDITORIA EN LA CARPETA G01 PARA EL USUARIO DEFAULTPOLICY
                PARA ELLO EN LA BUSQUEDA DE USUARIOS PONDREMOS LO SIGUIENTE IIS AppPool\DefaultAppPool Y AL BUSCARLO NOS LO ENCONTRARA
                POR OTRO LADO INICIAREMOS EL "INETMGR" Y EN LA SECCION DE EDITOR DE CONFIGURACION --> system.applicationHost/applicationPools
                Y EN LA TABLA EXPANDIMOS PROCESSMODELO: 
                
                 
                 - IDENTITY TYPE:ApplicationPoolIdentity
                 - 
                 */

                string ruta = string.Empty;

                HttpPostedFileBase file = Request.Files[0];

                //SIEMPRE LA POSICION 0 PORQUE UNICAMENTE SE SUBIRA UN FICHERO

                string path = "/productos";
                //PRUEBA
                //fileName = Path.GetFileName(file.FileName);

                //path = Path.Combine(Server.MapPath("~/productos/"), fileName);
                //file.SaveAs(path); 

                ////PRUEBA
                ruta = Server.MapPath("~") + "\\" + path;

                fileName = file.FileName;
                ruta = ruta.Replace("/", "\\");
                var nombrefichero = Path.GetFileName(file.FileName);
                if (!System.IO.File.Exists(ruta + @"\" + fileName))
                {
                    hecho = true;
                    file.SaveAs(ruta + @"\" + fileName);
                    mensaje = string.Format("La imagen '{0}' ha sido subida correctamente.", nombrefichero);
                }
                else
                {
                    hecho = false;
                    mensaje = string.Format("Imagen con nombre '{0}' ya existente.", nombrefichero);
                }

                //ESTA VARIABLE TIENE LA EXTENSION DEL FICHERO ADEMAS DE SU NOMBRE
                //CREAMOS UNA LISTA QUE CONTIENE UNA UNICA IMAGEN, ESTA LE INDICAMOS LOS VALORES DEL NUEVO FICHERO SUBIDO


            }
            catch (Exception ex)
            {
                throw new HttpException(ex.ToString());
            }
            //ENVIAMOS LOS NUEVOS DATOS DE LA IMAGEN SUBIDA A PROCESAR POR LA KENDOIMAGEBROWSER
            return Json(new { Mensaje = mensaje, Hecho = hecho }, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FileImages(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                path = Server.UrlDecode(path);
            }
            else
            {
                path = "/productos";
            }

            string ruta = string.Empty;
            if (!path.StartsWith("\\\\") && path.IndexOf(":\\") == -1)
            {
                ruta = Server.MapPath("~") + "\\" + path;
            }
            else
            {
                ruta = path;
            }
            ruta = ruta.Replace("/", "\\");

            if (!ruta.EndsWith("\\")) ruta += @"\";

            //List<Dictionary<string, object>> elementos = new List<Dictionary<string, object>>();

            List<ImagenesViewModel> Imagenes = new List<ImagenesViewModel>();
            var dirs = System.IO.Directory.GetDirectories(ruta);
            var files = System.IO.Directory.GetFiles(ruta);
            //for (int i = 0; i < dirs.Length; i++)
            //{
            //    Dictionary<string, object> dir = new Dictionary<string, object>();
            //    dir["uid"] = NewUid();
            //    dir["name"] = dirs[i].Substring(dirs[i].LastIndexOf('\\') + 1);
            //    dir["type"] = "d";
            //    dir["path"] = ruta;

            //    Imagenes.Add(dir);
            //}
            for (int x = 0; x < files.Length; x++)
            {
                //Dictionary<string, object> file = new Dictionary<string, object>();
                ImagenesViewModel file = new ImagenesViewModel();
                FileInfo f = new FileInfo(files[x]);
                file.name = files[x].Substring(files[x].LastIndexOf('\\') + 1);
                file.size = getFormatSize(f.Length);
                file.type = "f";
                file.path = ruta;
                string extension = Convert.ToString(file.name).Split('.').Last().ToUpper();
                if (extension == "BMP" || extension == "JPG" || extension == "JPEG" || extension == "PNG" || extension == "GIF" || extension == "ICO")
                {
                    Imagenes.Add(file);
                }

            }

            return View(Imagenes);
            //return new JsonResult() { Data = elementos, JsonRequestBehavior = JsonRequestBehavior.DenyGet };
        }

        public string getFormatSize(double len)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }
        private string NewUid()
        {
            string id = string.Empty;

            for (int i = 0; i < 32; i++)
            {
                Random random = new Random();
                int rnd = random.Next(0, int.MaxValue) * 16 | 0;

                if (i == 8 || i == 12 || i == 16 || i == 20)
                {
                    id += "-";
                }
                id += (i == 12 ? 4 : (i == 16 ? (rnd & 3 | 8) : rnd)).ToString("x2");
            }

            return id;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAlmacen(string RolUser)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.AppendFormat("SELECT * FROM {0} ORDER BY  IDPRODUCTO", "ALMACEN");
            List<Dictionary<string, object>> PRODUCTOSTABLA = new List<Dictionary<string, object>>();
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            PRODUCTOSTABLA = Conection.LeerDatos(SQL.ToString());
            List<AlmacenViewModel> ListadoAlmacen = new List<AlmacenViewModel>();

            foreach (var ProductoCoger in PRODUCTOSTABLA)
            {
                AlmacenViewModel Producto = new AlmacenViewModel();
                Producto.IdProducto = Convert.ToInt32(ProductoCoger["IDPRODUCTO"]);
                Producto.Nombre = Convert.ToString(ProductoCoger["NOMBRE"]);
                Producto.Descripcion = Convert.ToString(ProductoCoger["DESCRIPCION"]);
                Producto.Precio_Inicial = Convert.ToDouble(ProductoCoger["PRECIO_INICIAL"]);
                Producto.Imagen = Convert.ToString(ProductoCoger["IMAGEN"]);
                ListadoAlmacen.Add(Producto);
            }

            ViewBag.RolActual = RolUser;
            return View(ListadoAlmacen);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetTiendas(string RolUser)
        {
            StringBuilder SQL = new StringBuilder();
            SQL.AppendFormat("SELECT * FROM {0} ORDER BY  IDTIENDA", "TIENDA");
            List<Dictionary<string, object>> TIENDASTABLA = new List<Dictionary<string, object>>();
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            TIENDASTABLA = Conection.LeerDatos(SQL.ToString());
            List<TiendaViewModel> ListadoTiendas = new List<TiendaViewModel>();

            foreach (var TiendaCoger in TIENDASTABLA)
            {
                TiendaViewModel Tienda = new TiendaViewModel();
                Tienda.IdTienda = Convert.ToInt32(TiendaCoger["IDTIENDA"]);
                Tienda.Nombre = Convert.ToString(TiendaCoger["NOMBRE"]);
                Tienda.Direccion = Convert.ToString(TiendaCoger["DIRECCION"]);
                Tienda.Telefono = (TiendaCoger["TELEFONO"] == DBNull.Value) ? 000000000 : Convert.ToInt32(TiendaCoger["TELEFONO"]);
                Tienda.Correo = Convert.ToString(TiendaCoger["CORREO"]);

                ListadoTiendas.Add(Tienda);
            }

            ViewBag.RolActual = RolUser;
            return View(ListadoTiendas);
        }


        [ValidateAntiForgeryToken]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveGridUsers(List<IndexViewModel> UsuarioGuardar)
        {
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            StringBuilder SQL = new StringBuilder();
            bool hecho = false;
            string mensaje = string.Empty;
            if (UsuarioGuardar.Count > 0)
            {
                Type myType = UsuarioGuardar.FirstOrDefault().GetType();
                SQL.Clear();
                foreach (IndexViewModel Usuario in UsuarioGuardar)
                {
                    string[] nombreParams = null;// = new string[props.Count];
                    string[] valueParams = null; //= new string[props.Count];

                    if (!ModelState.IsValid)
                    {
                        mensaje = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                            .Select(e => e.ErrorMessage).ToList());
                        hecho = false;
                        break;
                    }

                    Conection.PrepareParametersSQL(myType, ref valueParams, ref nombreParams, Usuario);

                    SQL.AppendFormat("UPDATE USUARIOS SET ROL=@{0}, NOMBRE=@{1} , USUARIO = @{2} , TELEFONO=@{3},  DISABLED= @{4} WHERE IDUSER= @{5}",
                    nombreParams[0], nombreParams[1], nombreParams[2], nombreParams[3], nombreParams[4], nombreParams[5]);
                    Conection.EjecutarSQL(SQL.ToString(), nombreParams, valueParams);
                    /* SQL.AppendFormat("UPDATE USUARIOS SET USUARIO = '{0}', NOMBRE='{1}', DISABLED={2}, ROL={3} , TELEFONO={4} WHERE IDUSER={5} ;",
                         Usuario.NombreLogin, Usuario.NombreUser, (Usuario.Disabled) ? "1" : "0", Usuario.Rol, Usuario.Telefono, Usuario.Id);*/
                }
                if (string.IsNullOrEmpty(mensaje))
                {
                    hecho = true;
                }

            }

            return new JsonResult()
            {
                Data = new { Hecho = hecho, mensaje = mensaje },
                JsonRequestBehavior = JsonRequestBehavior.DenyGet
            };
        }

        [ValidateAntiForgeryToken]
        public ActionResult SaveCollectionProduct(List<AlmacenViewModel> ProductoGuardar)
        {
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            StringBuilder SQL = new StringBuilder();
            bool hecho = false;
            string mensaje = "";
            if (ProductoGuardar.Count > 0)
            {

                foreach (AlmacenViewModel Producto in ProductoGuardar)
                {
                    if (!ModelState.IsValid)
                    {
                        mensaje = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                            .Select(e => e.ErrorMessage).ToList());
                        hecho = false;
                        break;
                    }

                    SQL.AppendFormat("UPDATE ALMACEN SET NOMBRE = '{0}', PRECIO_INICIAL={1}, IMAGEN='{2}', DESCRIPCION='{3}' WHERE IDPRODUCTO={4} ;",
                      Producto.Nombre, Convert.ToString(Producto.Precio_Inicial).Replace(',', '.'), Producto.Imagen, Producto.Descripcion, Producto.IdProducto);

                }
                if (SQL != null)
                {
                    Conection.EjecutarSQL(SQL.ToString());
                    hecho = true;
                }
            }

            return new JsonResult()
            {
                Data = new { Hecho = hecho },
                JsonRequestBehavior = JsonRequestBehavior.DenyGet
            };
        }

        [ValidateAntiForgeryToken]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveGridShop(List<TiendaViewModel> TiendaGuardar)
        {
            string ConectionString = Conexion();
            Conection = new SQLServer(ConectionString);
            StringBuilder SQL = new StringBuilder();
            bool hecho = false;
            string mensaje = string.Empty;
            try
            {
                if (TiendaGuardar.Count > 0)
                {
                    Type myType = TiendaGuardar.FirstOrDefault().GetType();

                    SQL.Clear();

                    foreach (TiendaViewModel Tienda in TiendaGuardar)
                    {
                        // IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                        string[] nombreParams = null;// = new string[props.Count];
                        string[] valueParams = null; //= new string[props.Count];
                        EmailAddressAttribute email = new EmailAddressAttribute();

                        if (!ModelState.IsValid)
                        {
                            mensaje = string.Join(Environment.NewLine, ModelState.Values.SelectMany(v => v.Errors)
                                .Select(e => e.ErrorMessage).ToList());
                            hecho = false;
                            break;
                        }

                        if (Tienda.Correo != null && !email.IsValid(Tienda.Correo))
                        {
                            hecho = false;
                            mensaje = string.Format("Email '{0}' no válido", Tienda.Correo);
                            break;
                        }

                        Conection.PrepareParametersSQL(myType, ref valueParams, ref nombreParams, Tienda);
                        SQL.AppendFormat("UPDATE TIENDA SET NOMBRE = @{0}, DIRECCION=@{1}, TELEFONO= @{2}, CORREO=@{3} WHERE IDTIENDA= @{4}",
                            nombreParams[0], nombreParams[1], nombreParams[2], nombreParams[3], nombreParams[4]);
                        Conection.EjecutarSQL(SQL.ToString(), nombreParams, valueParams);
                    }

                    if (string.IsNullOrEmpty(mensaje))
                    {
                        hecho = true;
                    }
                }
            }
            catch (Exception ex)
            {

                mensaje = ex.Message;
            }
            return new JsonResult()
            {
                Data = new { Hecho = hecho, Mensaje = mensaje },
                JsonRequestBehavior = JsonRequestBehavior.DenyGet
            };
        }

        public string Conexion() //ESTABLECES CONEXION DESDE LA LECTURA DEL FICHERO WEB.CONFIG
        {
            System.Configuration.Configuration Config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/SharpAdmin");
            System.Configuration.ConnectionStringSettings Conexion;
            Conexion = Config.ConnectionStrings.ConnectionStrings[System.Web.Configuration.WebConfigurationManager.AppSettings["CadenaConexion"]];
            return Conexion.ConnectionString;
        }
    }
}