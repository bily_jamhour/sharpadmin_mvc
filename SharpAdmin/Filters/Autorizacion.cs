﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SharpAdmin.Filters
{
    public class Autorizacion : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["shlogin"] != null)
            {
                return true;
            }
            return base.AuthorizeCore(httpContext);
        }
    }
}