﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SharpAdmin.Startup))]
namespace SharpAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
