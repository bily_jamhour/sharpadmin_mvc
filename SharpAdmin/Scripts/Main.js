﻿//FALTA REMARCAR OPCION ANTERIORMENTE CLICADA EN LOS TABS
var App = {
    homeUrl: window.location.origin,
    elemento: "",
    Nombre: "",
    Rol: "",
    Login: "",
};
var Main = {
    edited: false,
    elemento: "",
    GridUpdate: {},
    CollectionUpdate: {},
    Init: function () {
        //        $(".button-collapse").sideNav();
        $('ul.tabs').tabs();
        $(".indicator").addClass("black");
        $("ul.tabs").find("li").bind("click", function () {
            Main.elemento = $(this).children();
            App.elemento = $(this).children();
            var opt = $(this).children().attr("value");
            // window.location.href = href;
            cargarOpciones(opt);

        });
        $(".brand-logo").bind("click", function () {
            location.reload();
        });
        //function load() {
        //    debugger
        //    if ($.trim(Main.elemento) != "") {
        //        elemento.parent().parent().removeClass("active");
        //        elemento.addClass("active");
        //        $('ul.tabs').tabs(elemento, elemento);
        //    }
        //}
        function cargarOpciones(idx) {
            var Opcion = "";
            var Valores = {};
            if (idx == "I") {
                $(".brand-logo").click();

            }
            else if (idx == "C") {
                Opcion = "/Home/Contact";
                // $.post(App.homeUrl + "/Home/Contact");
            }
            else if (idx == "M") {
                Opcion = "/Manage/Index";
                //   Valores = { Nombre: $("body").data("USR").Nombre, Rol: $("body").data("USR").Rol, Login: $("body").data("USR").Login }
            }
            else if (idx == "L") {
                Opcion = "/Cuenta/LoadFormulario";
            }
            if (idx != "I") {
                $.post(App.homeUrl + Opcion, Valores, function (Data) {
                    $(".container").empty();
                    $(".container").append(Data);

                    $("#conectarSesion").bind("click", function () {
                        if ($.trim($("#Password").val()) == "") {
                            Materialize.toast($("#Password").attr("data-val-required"), 3000);
                        }
                        else if ($.trim($("#Usuario").val()) == "") {
                            Materialize.toast($("#Usuario").attr("data-val-required"), 3000);
                        }
                        else {
                            $.post(App.homeUrl + "/Cuenta/Login", { __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val(), Usuario: $("#Usuario").val(), Password: $("#Password").val() }, function (Data) {
                                if ($(Data).is(".error")) {
                                    var content = $(Data);
                                    Materialize.toast(content.find("span"), 5000, 'red', function () {
                                        $(".error").remove();
                                        $("#Usuario").val("");
                                        $("#Password").val("");
                                    });
                                }
                                else if (Data.Envio) {


                                    location.reload();
                                    //LO VALORES DE LA SESIÓN ESTAN GUARDADO EN LA COOKIE!

                                    //$.post(App.homeUrl + "/Manage/Index", { Nombre: Data.Nombre, Rol: Data.Rol, Login: Data.Login }, function () {
                                    //    Main.Admin();
                                    //});
                                }
                            });

                        }
                    });
                    if (idx == "M") {
                        Main.Admin();

                    }
                });
            }
        }

        $("#closeSession").bind("click", function () {
            Main.GenerarModal("wndLogOut", String.format($("body").data("Idiomas").mensajes.mensajeModalSesion,"<br>"), "body", true, true);//"Va ha cerrar la sesión.<br> Realmente quiere salir de SharpAdmin?", "body", true, true);
            $("#wndLogOut").openModal({
                dismissible: false,
                ready: function () {
                    document.getElementById('alertSound').play();
                },
                complete: function () {
                    $("#wndLogOut").remove();
                }
            });

            $("#wndLogOut").find("#modalBtnAceptar").bind("click", function () {
                $("#wndLogOut").closeModal();
                $.post(App.homeUrl + "/Cuenta/LogOut", "", function (data) {
                    location.reload();
                });
            });
            $("#wndLogOut").find("#modalBtnCancelar").bind("click", function () {
                $("#wndLogOut").closeModal();
            });
        });
    },

    Admin: function () {//PARA LA SECCION MANAGE 
        $(".opcion").find("a").bind("click", function () {

            var idWnd = $(this).parents(".opcion").index();
            $(".container").find(".row").children().fadeOut();
            if (idWnd == 1) {
                $.post(App.homeUrl + "/Manage/GetUsuarios", { RolUser: $(".menuAplicacion").find(".active").data("Rol").Actual }, function (Data) {
                    $("#GetUsuarios").remove();
                    $(".container").find(".row").append(Data);
                    if ($(".menuAplicacion").find(".active").data("Rol").Actual == 1) {
                        Main.Usuarios();
                    }
                    else {
                        $(".roles").find("div").remove();
                        $("#volverOpciones").bind("click", function () {
                            Main.volverPanel(false);
                        });
                    }
                });
            }
            else if (idWnd == 2) {
                $.post(App.homeUrl + "/Manage/GetAlmacen", { RolUser: $(".menuAplicacion").find(".active").data("Rol").Actual }, function (Data) {
                    $("#GetAlmacen").remove();
                    $(".container").find(".row").append(Data);
                    if ($(".menuAplicacion").find(".active").data("Rol").Actual < 3) {
                        Main.Almacen();
                    }
                    else {
                        $("#volverOpciones").bind("click", function () {
                            Main.volverPanel(false);
                        });
                    }
                });
                Main.Almacen();
            }
            else if (idWnd == 3) {
                $.post(App.homeUrl + "/Manage/GetTiendas", { RolUser: $(".menuAplicacion").find(".active").data("Rol").Actual }, function (Data) {
                    $("#GetTiendas").remove();
                    $(".container").find(".row").append(Data);
                    if ($(".menuAplicacion").find(".active").data("Rol").Actual < 3) {
                        Main.Tiendas();
                    }
                    else {
                        $("#volverOpciones").bind("click", function () {
                            Main.volverPanel(false);
                        });
                    }
                });

            }

        });
    },
    Usuarios: function () {
        $("body").on("click.container", "*", function (e) {
            if ($(this).is("td") || $(this).is("span") || $(this).is(".edicion")) {
                e.stopPropagation();//PARAMOS LA PROPAGACION DE ESTE EVENTO, PARA ELLO SE LO INDICAMOS "e"
                //CON ESTO HACEMOS QUE EL EVENTO SOLO SE EJECUTE UNA VEZ EN CADA CLICK QUE SE HAGA Y NO ESCALE A LOS DEMAS ELEMENTOS QUE SE INCLUYAN EN NUESTRO CLICK
                //SI ESTO PASARIA EJECUTARIA LA ULTIMA CONDICION EN EL CUAL NOS LO DESMARCARIA
            }
            else {
                if ($(".mostrar").length > 0) {
                    Main.CerrarSelect();
                    //var value = $("input[data-activates='select-options-" + $("select").data("selectId") + "']").val();
                    //var id = $("select:first").find("option:contains('" + value + "')").val();
                    //$(".mostrar").prev().text($.trim(id));
                    //$(".mostrar").addClass("oculto");
                    //$(".mostrar").removeClass("mostrar");
                    //$(".oculto").prev().show();
                    //if (id != $.trim($(".oculto").prev().text())) {
                    //    $(".oculto").prev().addClass("changed");
                    //}
                }
                if ($(".edicion").length > 0) {
                    Main.CerrarInput();
                    //var element = $(".edicion").parent();
                    ////SI el valor que se ha introducido no es el mismo que esta recogido en BBDD, quedara como editado
                    //if ($.trim($(".edicion").val()) != "" && $.trim($(".edicion").val()) != $.trim(element.find("span").text())) {
                    //    element.find("span").text($.trim($(".edicion").val()));
                    //    element.find("span").addClass("changed");
                    //}
                    //element.find("span").show();
                    //$(".edicion").remove();
                }

            }
        });
        $("#GetUsuarios").find("td:not('.roles')").bind("click", function () {
            if ($(".mostrar").length > 0) {
                Main.CerrarSelect();
            }
            if ($(".edicion").length > 0 && $(this).find(".edicion").length == 0) { //SI YA HA EMPEZADO A EDITAR UNO
                Main.CerrarInput();
            }
            if ($(this).find("input").length == 0) {
                $(this).find("span").hide();
                $(this).append("<input class='edicion' maxlength='" + $(this).find("span").attr("maxlength") + "' type='text'></input>");
                $(".edicion").val($.trim($(this).find("span").text()));
            }
            else if ($(this).find("input[type='checkbox']").length > 0) {
                $(this).find("span").addClass("changed");
            }
        });
        $("#GetUsuarios").find("td.roles").bind("click", function () {
            if ($(".edicion").length > 0) {
                //var element = $(".edicion").parent();
                ////SI el valor que se ha introducido no es el mismo que esta recogido en BBDD, quedara como editado
                //if ($.trim($(".edicion").val()) != "" && $.trim($(".edicion").val()) != $.trim(element.find("span").text())) {
                //    element.find("span").text($.trim($(".edicion").val()));
                //    element.find("span").addClass("changed");
                //}
                //element.find("span").show();
                //$(".edicion").remove();
                Main.CerrarInput();
            }
            if ($(".mostrar").length > 0) {
                //var value = $("input[data-activates='select-options-" + $("select").data("selectId") + "']").val();
                //var id = $("select:first").find("option:contains('" + value + "')").val();
                //$(".mostrar").prev().text($.trim(id));
                //$(".mostrar").addClass("oculto");
                //$(".mostrar").removeClass("mostrar");
                //$(".oculto").prev().show();
                //if (id != $.trim($(".oculto").prev().text())) {
                //    $(".oculto").prev().addClass("changed");
                //}
                Main.CerrarSelect();
            }
            $(this).find("span:last").hide();
            var opcionSel = $(this).find("span:last").attr("value");
            $(this).find("div").removeClass("oculto");
            $(this).find(".input-field").addClass("mostrar");
            if (!$(this).find("div").find("select").is(".initialized")) {
                $(this).find("div").find("select").material_select(function (e) {

                    var value = $.trim($(".mostrar").find(".active span").text());
                    var id = $("select:first").find("option:contains('" + value + "')").val();
                    //var value = $("input[data-activates='select-options-" + $("select").data("selectId") + "']").val();
                    //var id = $("select:first").find("option:contains('" + value + "')").val();
                    $(".mostrar").next().attr("value", $.trim(id));
                    $(".mostrar").next().text($("select:first").find("option:contains('" + value + "')").text());
                    $(".mostrar").addClass("oculto");
                    if (id != $.trim($(".mostrar").prev().text())) {
                        $(".mostrar").next().addClass("changed");
                    }
                    $(".mostrar").next().show();
                    $(".mostrar").removeClass("mostrar");

                    $(".initialized").find("option").removeAttr("selected");
                    $(".initialized").find("option:contains('" + value + "')").attr("selected", "selected");
                    $(".initialized").material_select("destroy");


                });
            }
            $(this).find("option").removeAttr("selected");
            $(this).find("select option[value='" + opcionSel + "']").attr("selected", "selected");
            $("#select-options-" + $(".initialized").data("selectId")).prev().attr("value", $(this).find("span:last").text());
            // $(".mostrar").find("input").attr("value", )
        });
        $("#saveChangeGrid").bind("click", function () {
            if ($(".edicion").length > 0) {
                var element = $(".edicion").parent();
                if ($.trim($(".edicion").val()) != "") {
                    if ($.trim($(".edicion").val()) != $.trim(element.find("span").text())) {
                        Main.edited = true;//si valor de input y span no es el mismo, hay cambio, se guardará!
                    }
                    element.find("span").text($.trim($(".edicion").val()));
                    element.find("span").addClass("changed");
                }
                element.find("span").show();
                $(".edicion").remove();
            }
            if ($("#GetUsuarios").find(".changed").length > 0) {
                Main.edited = true; //EN EL CASO QUE SE HAYA CAMBIADO CON EL DROPDOWN
            }
            else {
                Main.edited = false;
            }
            if (Main.edited) {
                Main.LecturaModelos("GetUsuarios", "Users", Main.GridUpdate);
                Main.GridUpdate["__RequestVerificationToken"] = $("#GetUsuarios").find("input[name='__RequestVerificationToken']").val();

                $.post(App.homeUrl + "/Manage/SaveGridUsers", Main.GridUpdate, function (data) {
                    if (data.Hecho) {
                        Main.volverPanel(true, "Se han actualizado los datos de la sección USUARIOS");

                    }
                });
            }
            else {
                Materialize.toast("No hay cambios para almacenar", 2000);
            }
        });

        $("#volverOpciones").bind("click", function () {
            if ($(".changed").length > 0) {
                Main.GenerarModal("wndHayCambios", "Se han cambiado los valores en la tabla usuarios.Si continúa perderá los cambios aplicados.<br> Proceder?", "GetUsuarios", true, false);
                $("#wndHayCambios").openModal({
                    dismissible: false,
                    complete: function () { $("#wndHayCambios").remove(); }
                });
                $("#wndHayCambios").find("#modalBtnAceptar").bind("click", function () {
                    $(".changed").removeClass("changed");
                    Main.volverPanel(false);
                });

                $("#wndHayCambios").find("#modalBtnCancelar").bind("click", function () {
                    $("#wndHayCambios").closeModal();
                });
            }
            else {
                $("body").off("click");
                Main.volverPanel(false);
            }
        })
    },
    Almacen: function () {
        var ProductUpdate = [];
        $("#volverOpciones").bind("click", function () {
            if ((".edited").length > 0) {
                Main.GenerarModal("wndHayCambios", "Se han cambiado los valores en la tabla 'Almacen'.Si continúa perderá los cambios aplicados.<br> Proceder?", "GetAlmacen", true, false);
                $("#wndHayCambios").openModal({
                    dismissible: false,
                    complete: function () { $("#wndHayCambios").remove(); }
                });
                $("#wndHayCambios").find("#modalBtnAceptar").bind("click", function () {
                    $(".changed").removeClass("changed");
                    Main.volverPanel(false);
                });

                $("#wndHayCambios").find("#modalBtnCancelar").bind("click", function () {
                    $("#wndHayCambios").closeModal();
                });
            }
            else {
                Main.volverPanel(false);
            }
        });
        $(".seleccion").hide();
        $('.tooltipped').tooltip({ delay: 50 });
        $(".editarproducto").bind("click", function () {
            var fichaproducto = $(this).parent();
            var descripcionValor = $.trim(fichaproducto.find(".descripcion").text());
            var nombreValor = $.trim(fichaproducto.find(".title").text());
            var precioValor = $.trim(fichaproducto.find(".precio").text());
            var imagenValor = $.trim(fichaproducto.find(".imagen").attr("src"));
            var imagenFichero = $.trim(fichaproducto.find(".imagen").attr("alt"));
            //ASIGNACIONES DE VALORES A EDITAR
            $("#editProducto").find("#descripcion").val(descripcionValor);
            $("#editProducto").find("#nombre").val(nombreValor);
            $("#editProducto").find("#precio").val(precioValor);
            if (fichaproducto.find(".oculto").length == 0) {
                $("#editProducto").find("#imagen").attr("src", imagenValor);
                $("#editProducto").find("#imagen").attr("alt", imagenFichero);
            } else {
                $("#editProducto").find("#imagen").attr("src", "");
            }



            $("#editProducto").data("id", fichaproducto.find("#idProd").val());

            //ASIGNACIONES DE VALORES ANTIGUO PARA COMPARARLOS
            $("#editProducto").find("#descripcion").attr("oldValue", descripcionValor);
            $("#editProducto").find("#nombre").attr("oldValue", nombreValor);
            $("#editProducto").find("#precio").attr("oldValue", precioValor);
            $("#editProducto").find("#imagen").attr("oldValue", imagenValor);
            $("#editProducto").find("#imagen").attr("alt", imagenFichero)

            $("#editProducto").openModal({
                dismissible: false
            });
            $("#editProducto").find("input,textarea").bind("focusout", function () {
                if ($.trim($(this).attr("oldValue")) != $.trim($(this).val())) {
                    if ($.trim($(this).val()) == "") {
                        $(this).val($(this).attr("oldValue"));
                        $(this).parent().find("label").addClass("active");
                        $(this).removeClass("edited");
                    } else {
                        $(this).addClass("edited");
                    }

                }

            });
            $("#editProducto").find("#modalBtnAceptar").bind("click", function () {

                if ($("#editProducto").find(".edited").length > 0) {
                    var modeloProducto = $("#GetAlmacen").find("#idProd[value='" + $("#editProducto").data("id") + "']").parent();

                    if (modeloProducto.find(".oculto").length > 0 && $("#editProducto").find("#imagen").attr("src") != "") {
                        modeloProducto.find(".imagen").attr("src", $("#editProducto").find("#imagen").attr("src"));
                        modeloProducto.find(".imagen").attr("alt", $("#editProducto").find("#imagen").attr("alt"));
                        modeloProducto.find(".oculto").next().remove();
                        modeloProducto.find(".imagen").removeClass("oculto");

                    }
                    else if (modeloProducto.find(".imagen:not(.oculto)")) {
                        modeloProducto.find(".imagen").attr("src", $("#editProducto").find("#imagen").attr("src"));
                        modeloProducto.find(".imagen").attr("alt", $("#editProducto").find("#imagen").attr("alt"));
                    }
                    modeloProducto.find(".descripcion").text($("#editProducto").find("#descripcion").val());
                    if ($.trim($("#editProducto").find("#precio").val()) == "" || $("#editProducto").find("#precio").val() == "0") {
                        modeloProducto.find(".precio").text("0,1");
                    }
                    else {
                        modeloProducto.find(".precio").text($("#editProducto").find("#precio").val());
                    }
                    modeloProducto.find(".title b").text($.trim($("#editProducto").find("#nombre").val()));
                    modeloProducto.addClass("edited");
                }
                $("#editProducto").closeModal();
            });
            $("#editProducto").find("#modalBtnCancelar").bind("click", function () {
                $("#seleccionImagenes").remove();
                $("#editProducto").find(".input-field").show();
                $("#editProducto").closeModal();
            });
        });


        $("#subirimg").bind("click", function () {
            $("#subirImagen").click();
            //$.post(App.homeUrl + "/Manage/SubirImagen", "", function (Data) {
            //    Main.subirImagenes();
            //})
        });
        function ficherosubido(evt) {
            var files = evt.target.files; // FileList object
            if (files.length == 1) {
                var extension = files[0].name.split('.').pop();
                if (extension == "png" || extension == "jpeg" || extension == "jpg" || extension == "bmp") {
                    var formData = new FormData();
                    var file = files[0];
                    formData.append("FileUpload", file);
                    $.ajax({
                        type: "POST",
                        url: '/Manage/SubirImagen',
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            if (response.Hecho) {
                                Materialize.toast(response.Mensaje, 4000, "green");
                            }
                            else {
                                Materialize.toast(response.Mensaje, 4000, "red");
                            }
                        },
                        error: function (error) {
                            Materialize.toast("Error al subir la imagen seleccionada", 4000, "red");
                        }
                    });
                    //$.post(App.homeUrl + "/Manage/SubirImagen", { archivo: $("#subirImagen").val() }, function (Data) {
                    //    if (Data.length > 0) {
                    //        Materialize.toast(Data, 4000, "green");
                    //    }
                    //    else {
                    //        Materialize.toast("Error al subir la imagen seleccionada", 4000, "red");
                    //    }
                    //    Main.subirImagenes();
                    //});
                }
                else {
                    Materialize.toast("Fichero no válido.", 4000, "red");
                }
            }
            // files is a FileList of File objects. List some properties.
        }
        $("#subirImagen").change(function () {
            ficherosubido(event);
        });

        // document.getElementById('subirImagen').addEventListener('change', handleFileSelect, false);
        $("#aplicarimg").bind("click", function () {
            $.post(App.homeUrl + "/Manage/FileImages", "", function (Data) {
                if (Data.length > 0) {
                    Main.cargarImagenes(Data);
                    $("#imagenesProductos").find("li").bind("click", function () {

                        $("#imagenesProductos").find("li").removeClass("seleccionado");
                        $(this).addClass("seleccionado");
                        if ($("#imagenesProductos").find(".seleccionado").length > 0) {
                            var producto = $("#seleccionImagenes").find(".seleccionado");
                            $("#editProducto").find("#imagen").attr("src", producto.find("img").attr("src"));
                            $("#editProducto").find("#imagen").attr("alt", $.trim(producto.find(".title").text()));
                            $("#editProducto").find("#imagen").addClass("edited");
                        }
                        $("#seleccionImagenes").remove();
                        $("#editProducto .modal-content").find("div").show();
                    });
                }
            })
        });

        $("#saveChangeCollection").bind("click", function () {
            if ($("li.edited").length > 0) {
                Main.edited = true; //EN EL CASO QUE SE HAYA CAMBIADO CON EL DROPDOWN
            }
            if (Main.edited) {
                Main.LecturaModelos("GetAlmacen", "Almacen", Main.CollectionUpdate);
                Main.CollectionUpdate["__RequestVerificationToken"] = $("#GetAlmacen").find("input[name='__RequestVerificationToken']").val();
                debugger;
                $.post(App.homeUrl + "/Manage/SaveCollectionProduct", Main.CollectionUpdate, function (data) {
                    if (data.Hecho) {
                        Main.volverPanel(true, "Se han actualizado los datos de la sección ALMACÉN");

                    }
                });
            }
            else {
                Materialize.toast("No hay cambios para almacenar", 2000);
            }
        });
    },
    Tiendas: function () {
        $("body").on("click.container", "*", function (e) {
            if ($(this).is("td") || $(this).is("span") || $(this).is(".edicion")) {
                e.stopPropagation();//PARAMOS LA PROPAGACION DE ESTE EVENTO, PARA ELLO SE LO INDICAMOS "e"
                //CON ESTO HACEMOS QUE EL EVENTO SOLO SE EJECUTE UNA VEZ EN CADA CLICK QUE SE HAGA Y NO ESCALE A LOS DEMAS ELEMENTOS QUE SE INCLUYAN EN NUESTRO CLICK
                //SI ESTO PASARIA EJECUTARIA LA ULTIMA CONDICION EN EL CUAL NOS LO DESMARCARIA
            }
            else {
                if ($(".mostrar").length > 0) {
                    Main.CerrarSelect();
                }
                if ($(".edicion").length > 0) {
                    Main.CerrarInput();
                }

            }
        });
        $("#GetTiendas").find("td").bind("click", function () {
            if ($(".edicion").length > 0 && $(this).find(".edicion").length == 0) { //SI YA HA EMPEZADO A EDITAR UNO
                Main.CerrarInput();
            }
            if ($(this).find("input").length == 0) {
                $(this).find("span").hide();
                if ($(this).find(".Correo").length > 0) {
                    $(this).append("<input class='edicion validate' maxlength='" + $(this).find("span").attr("maxlength") + "' type='email'></input>");
                }
                else {
                    $(this).append("<input class='edicion' maxlength='" + $(this).find("span").attr("maxlength") + "' type='text'></input>");

                }
                $(".edicion").val($.trim($(this).find("span").text()));
            }
        });
        $("#saveChangeGrid").bind("click", function () {
            if ($(".edicion").length > 0) {
                var element = $(".edicion").parent();
                if ($.trim($(".edicion").val()) != "") {
                    if ($.trim($(".edicion").val()) != $.trim(element.find("span").text())) {
                        Main.edited = true;//si valor de input y span no es el mismo, hay cambio, se guardará!
                    }
                    element.find("span").text($.trim($(".edicion").val()));
                    element.find("span").addClass("changed");
                }
                element.find("span").show();
                $(".edicion").remove();
            }
            if ($("#GetTiendas").find(".changed").length > 0) {
                Main.edited = true; //EN EL CASO QUE SE HAYA CAMBIADO CON EL DROPDOWN
            }
            else {
                Main.edited = false;
            }
            if (Main.edited) {
                Main.LecturaModelos("GetTiendas", "Tiendas", Main.GridUpdate);
                Main.GridUpdate["__RequestVerificationToken"] = $("#GetTiendas").find("input[name='__RequestVerificationToken']").val();

                $.post(App.homeUrl + "/Manage/SaveGridShop", Main.GridUpdate, function (data) {
                    if (data.Hecho) {
                        Main.volverPanel(true, "Se han actualizado los datos de la sección TIENDAS");

                    }
                    else {
                        Materialize.toast(data.Mensaje, 4000, "red");
                    }
                });
            }
            else {
                Materialize.toast("No hay cambios para almacenar", 2000);
            }
        });

        $("#volverOpciones").bind("click", function () {
            if ($(".changed").length > 0) {
                Main.GenerarModal("wndHayCambios", "Se han cambiado los valores en la tabla 'Almacen'.Si continúa perderá los cambios aplicados.<br> Proceder?", "GetTiendas", true, false);
                $("#wndHayCambios").openModal({
                    dismissible: false,
                    complete: function () { $("#wndHayCambios").remove(); }
                });
                $("#wndHayCambios").find("#modalBtnAceptar").bind("click", function () {
                    $(".changed").removeClass("changed");
                    Main.volverPanel(false);
                });

                $("#wndHayCambios").find("#modalBtnCancelar").bind("click", function () {
                    $("#wndHayCambios").closeModal();
                });
            }
            else {
                $("body").off("click");
                Main.volverPanel(false);
            }
        });
    },
    subirImagenes: function () {
        $("#editProducto .modal-content").find("div").hide();
        $("#seleccionImagenes").remove();

    },
    cargarImagenes: function (Vista) {
        $("#editProducto .modal-content").find("div").hide();
        $("#seleccionImagenes").remove();
        $("#editProducto .modal-content").append(Vista);
        $("#seleccionImagenes").find("img[src='" + $("#imagen").attr("src") + "']").parents("li").addClass("seleccionado");

    },
    volverPanel: function (informativo, mensaje) {
        $(".container").find("div:visible:not(.row)").remove(); //el div de la opcion que se ha empezado ha editar
        $(".container .row").children().fadeIn();
        Main.edited = false;
        Main.GridUpdate = {};
        Main.CollectionUpdate = {};
        if (informativo) {
            Materialize.toast(mensaje, 4000, "green");
        }
    },
    GenerarModal: function (nombreModal, Alerta, Contenedor, Botones, Login) {
        if (Login) {
            $("<div id='" + nombreModal + "' class='modal modal-fixed-footer bottom-sheet'>" +
                "<nav class='modalTitle blue-grey darken-4'><div class='nav-wrapper'><a class='brand-logo center'>" + $("body").data("Idiomas").mensajes.tituloModalSesion + "</a></div></nav><div class='modal-content'><audio id='alertSound' src='1.mp3'></audio><p>" + Alerta + "</p></div><div class='modal-footer'>" +
            "<a href='#!' id='modalBtnCancelar' class=' modal-action modal-close waves-effect waves-red btn-flat'>" + $("body").data("Idiomas").mensajes.btnCancelar + "</a>" +
             "<a href='#!' id='modalBtnAceptar' class=' modal-action modal-close waves-effect waves-green btn-flat'>" + $("body").data("Idiomas").mensajes.btnAceptar + "</a></div></div>").appendTo($(Contenedor));
        }
        else if (Botones) {
            $("<div id='" + nombreModal + "' class='modal modal-fixed-footer'><div class='modal-content'><h4>SharpAdmin: Advertencia</h4><p>" + Alerta + "</p></div><div class='modal-footer'>" +
              "<a href='#!' id='modalBtnCancelar' class=' modal-action modal-close waves-effect waves-red btn-flat'>" + $("body").data("Idiomas").mensajes.btnCancelar + "</a>" +
               "<a href='#!' id='modalBtnAceptar' class=' modal-action modal-close waves-effect waves-green btn-flat'>" + $("body").data("Idiomas").mensajes.btnAceptar + "</a></div></div>").appendTo($("#" + Contenedor));

        }
        else {
            $("<div id='" + nombreModal + "' class='modal modal-fixed-footer'><div class='modal-content'><h4>SharpAdmin: Advertencia</h4><p>" + Alerta + "</p></div><div class='modal-footer'>" +
                 "<a href='#!' id='modalBtnAceptar' class=' modal-action modal-close waves-effect waves-green btn-flat'>" + $("body").data("Idiomas").mensajes.btnAceptar + "</a> </div></div>").appendTo($("#" + Contenedor));
        }

    },
    CerrarSelect: function () {
        //var value = $("input[data-activates='select-options-" + $("select").data("selectId") + "']").val();
        var value = $(".mostrar").find("option:contains('" + $(".mostrar").find("input").val() + "')").text();  //$(".mostrar").find("select option:selected").text();//$(".mostrar").find("select option:selected").val();
        //var id = $("select:first").find("option:contains('" + value + "')").val();
        $(".mostrar").next().text($.trim(value));
        $(".mostrar").addClass("oculto");
        if (value != $.trim($(".mostrar").prev().text())) {
            $(".mostrar").next().addClass("changed");
        }
        $(".mostrar").next().show();
        $(".mostrar").removeClass("mostrar");


    },
    CerrarInput: function () {
        var element = $(".edicion").parent();

        //SI el valor que se ha introducido no es el mismo que esta recogido en BBDD, quedara como editado
        if ($.trim($(".edicion").val()) != "" && $.trim($(".edicion").val()) != $.trim(element.find("span").text()) && $(".edicion").is(":not(.invalid)")) {
            element.find("span").text($.trim($(".edicion").val()));
            element.find("span").addClass("changed");
        }
        if ($(".invalid").length > 0) {
            Materialize.toast("Email no válido", 4000, "red", function () {
                $(".invalid").removeClass("invalid");
            });
        }


        element.find("span").show();
        $(".edicion").remove();
    },
    LecturaModelos: function (NombreGrid, Modalidad, Valores) {
        var Modelos = [];
        if (Modalidad == "Users") {
            var x = 0;

            $("#" + NombreGrid).find("tr").each(function (index) {
                var elementos = $(this).find("td");
                var value = "";
                var Modelo = {};
                var encontrado = false;
                if (elementos.length > 0) {
                    for (var i = 0; i < elementos.length ; i++) {
                        if ($(elementos[i]).find("span").is(".changed") && !encontrado) {
                            encontrado = true;
                        }
                        value = $.trim($(elementos[i]).find("span").text());
                        if (i == 0) {
                            value = $.trim($(elementos[i]).find("span").attr("value"));
                        }
                        if (i == 4) {//input checkbox
                            value = $(elementos[i]).find("input").is(":checked");
                        }
                        var prop = $($("#" + NombreGrid).find("th")[i]).attr("data-field");
                        Modelo[prop] = value;
                    }
                    Modelo["Id"] = $(this).find("#idUser").val();
                    if (encontrado) {
                        Modelos.push(Modelo);
                    }
                    x++;
                }
            });
            for (var i = 0 ; i < Modelos.length; i++) {
                for (var prop in Modelos[i]) {//montamos el modelo correctamente para que se pueda tratar como "IndexViewModel"
                    Valores["UsuarioGuardar[" + i + "]." + prop] = Modelos[i][prop];
                    //SOLO GUARDAMOS LAS PROPIEDADES QUE NOS INTERESEN PARA BORRAR CADA UNO DE ELLOS
                }
            }
        }
        else if (Modalidad == "Almacen") {
            var x = 0;

            $("#" + NombreGrid).find("li.edited").each(function (index) {
                var Producto = $(this);
                var Modelo = {};
                Modelo["Nombre"] = $.trim($(this).find(".title").text());
                Modelo["Precio_Inicial"] = $.trim($(this).find(".precio").text()).replace('.', ',');
                Modelo["Imagen"] = $.trim($(this).find(".imagen").attr("alt"));
                Modelo["Descripcion"] = $.trim($(this).find(".descripcion").text());
                Modelo["IdProducto"] = $.trim($(this).find("input[type='hidden']").val());
                Modelos.push(Modelo);
            });
            for (var i = 0; i < Modelos.length; i++) {
                for (var prop in Modelos[i]) {//montamos el modelo correctamente para que se pueda tratar como "IndexViewModel"
                    Valores["ProductoGuardar[" + i + "]." + prop] = Modelos[i][prop];
                    //SOLO GUARDAMOS LAS PROPIEDADES QUE NOS INTERESEN PARA BORRAR CADA UNO DE ELLOS
                }
            }
        }
        else if (Modalidad == "Tiendas") {
            $("#" + NombreGrid).find("tr").each(function (index) {
                var elementos = $(this).find("td");
                var value = "";
                var Modelo = {};
                var encontrado = false;
                if (elementos.length > 0) {
                    for (var i = 0; i < elementos.length ; i++) {
                        if ($(elementos[i]).find("span").is(".changed") && !encontrado) {
                            encontrado = true;
                        }
                        value = $.trim($(elementos[i]).find("span").text());
                        var prop = $($("#" + NombreGrid).find("th")[i]).attr("data-field");
                        Modelo[prop] = value;
                    }
                    Modelo["IdTienda"] = $(this).find("#idShop").val();
                    if (encontrado) {
                        Modelos.push(Modelo);
                    }
                    x++;
                }
            });
            for (var i = 0 ; i < Modelos.length; i++) {
                for (var prop in Modelos[i]) {//montamos el modelo correctamente para que se pueda tratar como "IndexViewModel"
                    Valores["TiendaGuardar[" + i + "]." + prop] = Modelos[i][prop];
                    //SOLO GUARDAMOS LAS PROPIEDADES QUE NOS INTERESEN PARA BORRAR CADA UNO DE ELLOS
                }
            }
        }
        else if (Modalidad == "Stock") {

        }

    }
};


$(function () {
    Main.Init();
    Main.Admin();
});
